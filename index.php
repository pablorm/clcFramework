<?php
	const APLICATION_MODE = 0;

	if(APLICATION_MODE==1){
		define('SERVER_ROOT' , '/var/www');
	}elseif(APLICATION_MODE==0 || APLICATION_MODE==2){
		define('SERVER_ROOT' , 'D:/AppServ/www/clcframework');
	}

	//ini_set('upload_tmp_dir', SERVER_ROOT."/tempdir"); 
	//ini_set('memory_limit', "-1"); 
	ini_set('memory_limit', '-1');
	ini_set('set_time_limit', '0');

	require_once(SERVER_ROOT . '/libraries/drivers/oracleimproved.php');
	require_once(SERVER_ROOT . '/application/' . 'Config.php');
	require_once(SERVER_ROOT . '/application/' . 'Controller.php');
	require_once(SERVER_ROOT . '/application/' . 'Session.php');
	require_once(SERVER_ROOT . '/application/' . 'View.php');
	require_once(SERVER_ROOT . '/application/' . 'Model.php');
	require_once(SERVER_ROOT . '/application/' . 'Router.php');

?>